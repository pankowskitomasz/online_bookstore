import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BooksRoutingModule } from './books-routing.module';
import { BooksComponent } from './books/books.component';
import { BookbarComponent } from './bookbar/bookbar.component';
import { BooklistComponent } from './booklist/booklist.component';


@NgModule({
  declarations: [
    BooksComponent,
    BookbarComponent,
    BooklistComponent
  ],
  imports: [
    CommonModule,
    BooksRoutingModule
  ]
})
export class BooksModule { }
