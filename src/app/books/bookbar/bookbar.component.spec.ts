import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BookbarComponent } from './bookbar.component';

describe('BookbarComponent', () => {
  let component: BookbarComponent;
  let fixture: ComponentFixture<BookbarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BookbarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BookbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
